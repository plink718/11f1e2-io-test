# 11f1e2-io-test

#### 介绍
UART和GPIO的应用示例程序，南京沁恒SPI转UART芯片CH9434，硬件接口如下：
![硬件接口](8d38f6836d5f5fcd710f34f148e2bd5.png)
接口信号定义见产品手册。
产品手册：[http://www.plink-ai.com/Uploads/download/11F1E2_Datasheet_CN_v2.pdf](http://www.plink-ai.com/Uploads/download/11F1E2_Datasheet_CN_v2.pdf)

需注意，此产品GPIO为光耦隔离IO，In代表GPI，只能用作输入，On代表GPO，只能用作输出。(n为硬件接口图上的数字)

#### 程序说明 
 **gpio_test_l.c**  设置  **GPO**  输出状态为 **低** ，同时循环读取  **GPI** 输入状态，读取30次过后，程序运行结束。

 **gpio_test_h.c**  设置  **GPO**  输出状态为 **高** ，同时循环读取  **GPI** 输入状态，读取30次过后，程序运行结束。

 **tty_test4c11.c** 用于进行串口收发测试。

#### 使用说明

编译程序：
```
gcc  tty_test4c11.c  -o  tty_test4c11
gcc  gpio_test_l.c  -o  gpio_test_l
gcc  gpio_test_h.c  -o  gpio_test_h
```

设置 **GPO** 状态：
```
sudo ./gpio_test_h    #设置GPIO输出状态为高
sudo ./gpio_test_l    #设置GPIO输出状态为低
```

获取 **GPI** 状态：
gpio_test_h和gpio_test_l都可以读取到 **GPI** 的输入状态，任选其一执行即可。

串口测试：
```
sudo ./tty_test4c11 -D /dev/ttyWCH0    #设备名以实际使用端口设备名为准。
```


