# 11f1e2-io-test

#### Introduce
UART and GPIO application example program, Nanjing Qincheng SPI to UART chip CH9434, the hardware interface is as follows:
![hardware interface](8d38f6836d5f5fcd710f34f148e2bd5.png)
Interface signals are defined in the product manual.
Datasheet：[http://www.plink-ai.com/Uploads/download/11F1E2_Datasheet_Chinese_V1.1.pdf](http://www.plink-ai.com/Uploads/download/11F1E2_Datasheet_Chinese_V1.1.pdf)

Note that the GPIO of this product is optocoupler isolated IO, In stands for GPI, which can only be used as input, and On stands for GPO, which can only be used as output. (n is the number on the hardware interface diagram)

#### Program code specification 
 **gpio_test_l.c** Set the output state of **GPO** to **low**, and read the input state of **GPI** in a loop. After reading 30 times, the program ends.

 **gpio_test_h.c**  Set the output state of **GPO** to **high**, while reading the input state of **GPI**, after reading 30 times, the program is finished.

 **tty_test4c11.c** Used to test sending and receiving serial ports。

#### Instructions

Compile program：
```
gcc  tty_test4c11.c  -o  tty_test4c11
gcc  gpio_test_l.c  -o  gpio_test_l
gcc  gpio_test_h.c  -o  gpio_test_h
```

Set **GPO** status：
```
sudo ./gpio_test_h    #Set the GPIO output status to high
sudo ./gpio_test_l    #Set the GPIO output status to low
```

Get **GPI** status：
Set the GPIO output state to high gpio_test_h and gpio_test_l can both read the input state of **GPI**.

Serial port test：
```
sudo ./tty_test4c11 -D /dev/ttyWCH0    #The device name depends on the device name of the port in use.
```
