/*
 * Y-C11 Isolation GPIO application example
 *
 * Copyright (C) 2023  Beijing Plink Ai Technology Co.,LTD.
 * Web: http://www.plink-ai.com
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License.
 *
 *
 * Version: V1.2
 */
 
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <unistd.h>
#include <fcntl.h>  
#include <errno.h>   
#include <string.h>
#include <sys/types.h> 
#include <sys/stat.h>
#include <sys/ioctl.h>

#define IOCTL_MAGIC 'W'
#define IOCTL_CMD_GPIOSET		_IOW(IOCTL_MAGIC, 0x84, uint16_t)
#define IOCTL_CMD_GPIOGET		_IOWR(IOCTL_MAGIC, 0x85, uint16_t)

#define OUTPUT_GROUP	0x1
#define INPUT_GROUP	0x2
#define HIGH	1
#define LOW	0



typedef union GPIO_U{
        unsigned char GValue;
        struct{
                unsigned char unused1:1;
                unsigned char gpio1:1;
                unsigned char gpio2:1;
                unsigned char gpio3:1;
                unsigned char unused2:1;
                unsigned char gpio4:3;

        }gpio;
}GpioGroupValue;





int set_gpio_value_by_group(int fd, unsigned char gpiobits)
{
    unsigned long val = (OUTPUT_GROUP << 8) | gpiobits;

    return ioctl(fd, IOCTL_CMD_GPIOSET, &val);
}


int get_gpio_value_by_group(int fd,  unsigned char *gpioval)
{
    unsigned long val = INPUT_GROUP << 8;

    if (ioctl(fd, IOCTL_CMD_GPIOGET, &val) != 0)
    {
	printf("Can't get the gpio's value\n");
        return -1;
    }
		
    *gpioval = (unsigned char)val;

    return 0;
}

void print_input_gpios_states(unsigned char gval)
{
	printf("GPIO Input Group value = 0x%02x\n", gval);

        printf("GPIO-In-01=%s\n",(gval&(1<<0))?"Low":"High");
        printf("GPIO-In-02=%s\n",(gval&(1<<1))?"Low":"High");
        printf("GPIO-In-03=%s\n",(gval&(1<<2))?"Low":"High");
        printf("GPIO-In-04=%s\n",(gval&(1<<3))?"Low":"High");
}









int main(int argc, char *argv[])
{
	int fd;
	int ret;
	unsigned char gpioval = 0;
 	GpioGroupValue outputvalue;

	if(getgid()!=0)
	{
		printf("\n This program requires root privilege !\n");	
		return -1;
	}

	fd = open("/dev/plink-gpios", O_RDWR);
	if (fd < 0)
	{	
		printf("can't open device!\n");
		return -1;
	}


	//set OUT-gpio1 and OUT-gpio3 output to high
	outputvalue.gpio.gpio1 = HIGH;
	outputvalue.gpio.gpio3 = HIGH;

	//set OUT-gpio2 and OUT-gpio4 output to low
	outputvalue.gpio.gpio2 = HIGH;
	outputvalue.gpio.gpio4 = HIGH;

	//write output setting to harware
	set_gpio_value_by_group(fd, outputvalue.GValue);
	
	


	for(int i=0; i<60;i++)
	{//show input gpio's value		
		ret = get_gpio_value_by_group(fd, &gpioval);
		if(ret != 0){
			return -1;
		}else {
			printf("i=%d, ", i);
			print_input_gpios_states(gpioval);
		}

		sleep(1);
	}	


	if(fd)
	{
		close(fd);
	}
}

